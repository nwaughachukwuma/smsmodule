/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms.web.dwr;

import java.util.Date;

import org.openmrs.module.sms.model.SmsProvider;

/**
 * @author crecabarren
 *
 */
public class SmsProviderListItem {
	
	private Integer id;
	private String name;
	private String address;
	private String username;
	private String password;
	private boolean preferred;
	private boolean voided;
	private Date createdAt;
	private Date updatedAt;
	private Date voidedAt;
	
	public SmsProviderListItem() {}
	/**
	 * @param provider
	 */
	public SmsProviderListItem(SmsProvider provider) {
		this.id = provider.getId();
		this.name = provider.getName();
		this.address = provider.getAddress();
		this.username = provider.getUsername();
		this.password = provider.getPassword();
		this.preferred = provider.isPreferred();
		this.voided = provider.isVoided();
		this.createdAt = provider.getCreatedAt();
		this.updatedAt = provider.getUpdatedAt();
		this.voidedAt = provider.getVoidedAt();
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the preferred
	 */
	public boolean isPreferred() {
		return preferred;
	}
	/**
	 * @param preferred the preferred to set
	 */
	public void setPreferred(boolean preferred) {
		this.preferred = preferred;
	}
	/**
	 * @return the voided
	 */
	public boolean isVoided() {
		return voided;
	}
	/**
	 * @param voided the voided to set
	 */
	public void setVoided(boolean voided) {
		this.voided = voided;
	}
	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}
	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}
	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	/**
	 * @return the voidedAt
	 */
	public Date getVoidedAt() {
		return voidedAt;
	}
	/**
	 * @param voidedAt the voidedAt to set
	 */
	public void setVoidedAt(Date voidedAt) {
		this.voidedAt = voidedAt;
	}
	
}
