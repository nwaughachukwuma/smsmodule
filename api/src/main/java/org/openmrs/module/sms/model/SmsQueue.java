package org.openmrs.module.sms.model;

import java.io.Serializable;
import java.sql.Timestamp;

import org.openmrs.BaseOpenmrsObject;

public class SmsQueue extends BaseOpenmrsObject implements Serializable
{
	private static final long serialVersionUID = 2362717782133670884L;
	private Integer id;
	private Integer programmingId;
	private Integer patientId;
	private Integer numberAttempt = 0;
	private Timestamp lastAttemptDateTime;
	private Integer obsId;

	public Integer getObsId() {
		return obsId;
	}

	public void setObsId(Integer obsId) {
		this.obsId = obsId;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
		
	}

	public Integer getProgrammingId() {
		return programmingId;
	}

	public void setProgrammingId(Integer programmingId) {
		this.programmingId = programmingId;
	}

	public Integer getPatientId() {
		return patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public Integer getNumberAttempt() {
		return numberAttempt;
	}

	public void setNumberAttempt(Integer numberAttempt) {
		this.numberAttempt = numberAttempt;
	}

	public Timestamp getLastAttemptDateTime() {
		return lastAttemptDateTime;
	}

	public void setLastAttemptDateTime(Timestamp lastAttemptDateTime) {
		this.lastAttemptDateTime = lastAttemptDateTime;
	}
	

}
