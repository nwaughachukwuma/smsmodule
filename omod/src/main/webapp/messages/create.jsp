<%--
Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.

Reproduction or distribution of this source code is prohibited.
--%>
<%@ include file="/WEB-INF/template/include.jsp" %>

<script>
function makeMessageCreatePanel(){
	return new Ext.Panel({
		frame : true,
		border : false,
		bodyStyle : "padding : 5px",
		autoHeight: true,
		autoWidth: true,
		items: [{
			xtype : "form",
			id : "message-form",
			name : "message-form",
			items : [{
				xtype : "hidden",
				id : "message-form-id",
				name : "id"
			},{
				xtype : "fieldset",
				title : '<spring:message javaScriptEscape="true" code="sms.text" />',
				layout : "column",
				items : [{
					layout : "form",
					border : false,
					columnWidth : 0.5,
					items : [{
						xtype : "textfield",
						id : "message-form-name",
						name : "name",
						fieldLabel : '<spring:message javaScriptEscape="true" code="sms.name" />'
					}]
				},{
					layout : "form",
					border : false,
					columnWidth : 0.5,
					items : [{
						xtype : "textarea",
						id : "message-form-text",
						name : "text",
						fieldLabel : '<spring:message javaScriptEscape="true" code="sms.text" />',
						width: 200
					}]
				}]
			},{
				xtype : "fieldset",
				title : '<spring:message javaScriptEscape="true" code="sms.sending" />',
				layout : "column",
				items : [{
					layout : "form",
					border : false,
					columnWidth : 0.3,
					items : [
						new Ext.ux.form.SpinnerField({
							id : "message-form-delay",
							name : "delay",
							width : 50,
							minValue : -100,
							maxValue : 365
						}),
					]
				},{
					layout : "form",
					border : false,
					columnWidth : 0.6,
					items : [{
						xtype : 'hidden',
						id : 'message-form-event-id',
						name : 'message-form-event-id'
					},{
						xtype : "combo",
						id : "message-form-event",
						fieldLabel : '<spring:message javaScriptEscape="true" code="sms.days.after" />',
						name : "event",
						store: new Ext.data.Store({
					        proxy: new Ext.ux.data.DwrProxy({
					        	apiActionToHandlerMap : {
					        		read : {
				        				dwrFunction : DWRConceptService.findConcepts,
				        				getDwrArgsFunction: function() {
											return [
												Ext.getCmp('message-form-event').getValue(),
												false,
												[],[],[],[],false
											];
										}
					        		}
					        	}
					        }),
					        reader: new Ext.data.JsonReader({
					            idProperty : 'id',
					            fields : [
					                      {name : 'conceptId'},
					                      {name : 'name'},
					                      {name : 'description'}
								]}),
					    }),
				        displayField: 'name',
				        typeAhead: false,
				        loadingText: 'Searching...',
				        width: 300,
				        pageSize:10,
				        hideTrigger:true,
				        tpl: new Ext.XTemplate(
				                '<tpl for="."><div class="search-item">',
				                '<h3>{name}</h3>',
				            	'</div></tpl>'
				        ),
				        itemSelector: 'div.search-item',
				        onSelect: function(combo, record, index){
							Ext.getCmp('message-form-event').setValue(combo.data.name);
							Ext.getCmp('message-form-event-id').setValue(combo.data.conceptId);
							Ext.getCmp('message-form-event').collapse();
				        }
					},{
						layout : "form",
						border : false,
						columnWidth : 0.5,
						items : [{
							xtype : "textarea",
							id : "message-form-sql",
							name : "sql",
							fieldLabel : '<spring:message javaScriptEscape="true" code="sms.sql" />',
							width: 200
						}]
					}]
				}]
			},{
				xtype : "fieldset",
				title : '<spring:message javaScriptEscape="true" code="sms.criteria" />',
				layout : "column",
				items : [{
					layout : "form",
					border : false,
					columnWidth : 1,
					items : [{
						xtype : "hidden",
						id : 'message-form-cohort',
						name : 'cohort'
					},{
						xtype : "grid", 
						id : "message-form-criteria",
						name : "criteria",
						width : 600,
						autoHeight : true,
						colModel : new Ext.grid.ColumnModel([
							{id : 'id', header : '<spring:message javaScriptEscape="true" code="sms.id" />', width : 30, sortable : false, dataIndex : 'id'},
							{id : 'name', header : '<spring:message javaScriptEscape="true" code="sms.name" />', width : 80, sortable : false, dataIndex : 'name'},
							{id : 'description', header : '<spring:message javaScriptEscape="true" code="sms.description" />', width: 200, sortable: false, dataIndex : 'description'}]),
						store : Ext.StoreMgr.lookup('cohort-store'),
						selModel : new Ext.grid.RowSelectionModel({
							singleSelect: true,
							listeners: {
								rowselect: function(sm, row, rec) {
									Ext.getCmp("message-form-cohort").setValue(rec.id);
								}
							}
						})
					}]
				}]
			}],
			buttons : [{
				id : "message-form-save",
				text : '<spring:message javaScriptEscape="true" code="sms.save" />',
				handler : function(){
					if(Ext.getCmp("message-form").getForm().isValid()){
						var store = Ext.StoreMgr.lookup('message-store');
						var formEvent = Ext.getCmp("message-form-event").getValue();
						var formSql = Ext.getCmp("message-form-sql").getValue();
						if (formEvent != null && formEvent != "" && formSql != null && formSql != ""){
							alert("Cannot Both Day After & SQL have value, please clear Day After.");
							return;
						}
					
						if(Ext.getCmp("message-form-id").getValue() != ""){
							var record = store.getById(Ext.getCmp("message-form-id").getValue());
							record.beginEdit();
							record.set("name", Ext.getCmp("message-form-name").getValue());
							record.set("text", Ext.getCmp("message-form-text").getValue());
							record.set("delay", Ext.getCmp("message-form-delay").getValue());
							record.set("event", formEvent);
							record.set("sql", formSql);
							record.endEdit();
							record.commit();
						} else {
							var arr = new store.recordType({
								name : Ext.getCmp("message-form-name").getValue(),
								text : Ext.getCmp("message-form-text").getValue(),
								delay : Ext.getCmp("message-form-delay").getValue(),
								event : formEvent,
								sql : formSql
							});
							try{
								store.add(arr);
							}catch(e){
								alert(e.toSource());
							}
						}
					}
				}
			},{
				id : "message-form-delete",
				text : '<spring:message javaScriptEscape="true" code="sms.reset" />',
				handler : function(){
					Ext.MessageBox.confirm('Confirm', 'Are you sure?', function(btn) {
						if (btn == 'yes') {
							var store = Ext.StoreMgr.lookup('gateway-store');
							if(Ext.getCmp("message-form-id").getValue() != ""){
								var record = store.getById(Ext.getCmp('message-form-id').getValue());
								store.remove(record);
							}
							else{
								Ext.getCmp("message-form").getForm().reset();
							}
						}
					});
				}
			}]
		}]
	});
}
</script>