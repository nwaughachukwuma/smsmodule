/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.openmrs.BaseOpenmrsObject;

/**
 * A Class to store sms and features to trigger the send of them.
 * 
 * @author crecabarren
 * @since 0.1
 */
public class PatientListSmsTestListItem extends BaseOpenmrsObject implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8014438026867785906L;
	private Integer id;
	private Integer patientId;
	private String personName;
	private String displayName;
	/**
	 * @return the patientId
	 */
	public Integer getPatientId() {
		return patientId;
	}
	/**
	 * @param patientId the patientId to set
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}
	/**
	 * @return the personName
	 */
	public String getPersonName() {
		return personName;
	}
	/**
	 * @param personName the personName to set
	 */
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return this.id;
	}
	@Override
	public void setId(Integer id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	
}
