/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms.model;

import java.io.Serializable;

import org.openmrs.BaseOpenmrsObject;

/**
 * @author crecabarren
 *
 */
public class SmsEvent extends BaseOpenmrsObject implements Serializable  {

	private static final long serialVersionUID = 38748429732234983L;
	private Integer id;
	private String name;
	private Integer priority;
	
	/**
	 * @see org.openmrs.OpenmrsObject#getId()
	 */
	public Integer getId() {
		return this.id;
	}
	/**
	 * @see org.openmrs.OpenmrsObject#setId(java.lang.Integer)
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the priority
	 */
	public Integer getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	
}
