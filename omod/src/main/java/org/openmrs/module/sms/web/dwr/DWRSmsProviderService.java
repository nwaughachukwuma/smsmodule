/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms.web.dwr;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.module.sms.api.SmsService;
import org.openmrs.module.sms.model.SmsProvider;

/**
 * A DWR Service, to provide a JavaScript class to be used in a JSP page.
 * @author crecabarren
 * @since 0.1
 */
public class DWRSmsProviderService {
	
	protected Log log = LogFactory.getLog(getClass());
	private SmsService service = Context.getService(SmsService.class);
	
	/**
	 * A method to the list of providers, as a ListItem Object, to prevent the direct access.
	 * @return List of providers
	 */
	public List<SmsProviderListItem> getProviders(){
		List<SmsProviderListItem> providers = new ArrayList<SmsProviderListItem>();
		if(Context.isAuthenticated()){
			for (SmsProvider prv : service.getAllProviders()){
				providers.add(new SmsProviderListItem(prv));
			}
		}
		return providers;
	}
	
	/**
	 * A method to store a list of SmsProviders items, each item are sent by the web browser as a JSON object and DWR api
	 * convert it to a SmsProviderListItem object.
	 * @param item, the list of items to be stored.
	 * @return
	 */
	public List<SmsProviderListItem> saveOrUpdate(List<SmsProviderListItem> item){
		List<SmsProviderListItem> list = new ArrayList<SmsProviderListItem>();
		if(Context.isAuthenticated()){
			for (SmsProviderListItem obj : item){
				SmsProvider provider;
				if(obj.getId() != null){
					provider = service.getProviderById(obj.getId());
				}
				else{
					provider = new SmsProvider();
				}
				provider.setName(obj.getName());
				provider.setAddress(obj.getAddress());
				provider.setUsername(obj.getUsername());
				provider.setPassword(obj.getPassword());
				provider.setPreferred(obj.isPreferred());
				provider.setVoided(obj.isVoided());
				service.createProvider(provider);
			}
		}
		return list;
	}
	
	/**
	 * 
	 * @param item
	 */
	public void deleteProvider(List<SmsProviderListItem> item){
		if(Context.isAuthenticated()){
			for(SmsProviderListItem obj : item){
				SmsProvider provider = service.getProviderById(obj.getId());
				service.deleteProvider(provider);
			}
		}
	}

}
