/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */

package org.openmrs.module.sms.api;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.openmrs.api.OpenmrsService;
import org.openmrs.module.sms.api.db.SmsModuleDAO;
import org.openmrs.module.sms.model.SmsHistory;
import org.openmrs.module.sms.model.SmsProgramming;
import org.openmrs.module.sms.model.SmsProvider;
import org.openmrs.module.sms.model.SmsQueue;
import org.springframework.transaction.annotation.Transactional;
import org.openmrs.Patient;

/**
 * A service class to perform actions like, get and store objects and lists of them.
 * @author crecabarren
 * @since 0.1
 */
@Transactional()
public interface SmsService extends OpenmrsService {
	
	/**
	 * A method used by OpenMRS api to set the Data Access Object (DAO).
	 * @param dao
	 */
	public void setSmsDAO(SmsModuleDAO dao);
	
	/**
	 * A method to create a new (or update an old) a provider.
	 * @param provider
	 */
	public SmsProvider createProvider(SmsProvider provider);
	
	/**
	 * 
	 * @param provider
	 */
	public void deleteProvider(SmsProvider provider);
	
	/**
	 * 
	 * @return
	 */
	public List<SmsProvider> getAllProviders();
	
	/**
	 * 
	 * @return
	 */
	public List<SmsProvider> getActiveProviders();
	
	/**
	 * 
	 * @return
	 */
	public List<SmsProvider> getPreferredProvider();
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public SmsProvider getProviderById(Integer id);
	
	/**
	 * 
	 * @return
	 */
	public List<SmsProgramming> getAllMessages();
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public SmsProgramming getMessageById(Integer id);
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public List<SmsProgramming> getMessagesByKeyWord(String key);
	
	/**
	 * 
	 * @param programming
	 * @return
	 */
	public SmsProgramming createMessage(SmsProgramming programming);
	
	/**
	 * 
	 * @param programming
	 */
	public void deteleMessage(SmsProgramming programming);
	
	/**
	 * 
	 * @param history
	 */
	public void createHistory(SmsHistory history);

	/**
	 * 
	 * @param history
	 */
	public SmsHistory getHistory(Patient patient, SmsProgramming programming);
	
	public List<SmsHistory> getListHistory(Patient patient, SmsProgramming programming);
        
    public List<SmsHistory> getListHistory(SmsProgramming programming);
	
	public List<Integer> getListObservationFromConceptAndPatient(Patient patient, SmsProgramming programming);
        
    public List getListObservationFromConcept(SmsProgramming programming);
    
    public List getListObservationFromProgramming(SmsProgramming programming, List<Integer> listPatient, boolean sendRepeat);
	
	public SmsHistory createSmsHistory(Patient patient, SmsProgramming sms, String coderr, String deserr, Integer obs_id);

	public Date getDateSQL(String sql, Integer patientId);
	
	public List<SmsQueue> getAllSmsQueue();
	
	public void saveSmsQueue(SmsQueue smsQueue);
	
	public void deleteSmsQueue(SmsQueue smsQueue);
        
	public SmsQueue getSmsQueueById(Integer id);
	
	public void deleteAllSmsQueue();
}
