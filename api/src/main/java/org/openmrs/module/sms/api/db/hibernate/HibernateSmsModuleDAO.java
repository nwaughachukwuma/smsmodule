/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */

package org.openmrs.module.sms.api.db.hibernate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.openmrs.Patient;
import org.openmrs.module.sms.api.db.SmsModuleDAO;
import org.openmrs.module.sms.model.SmsHistory;
import org.openmrs.module.sms.model.SmsProgramming;
import org.openmrs.module.sms.model.SmsProvider;
import org.openmrs.module.sms.model.SmsQueue;

/**
 * @author crecabarren
 *
 */
public class HibernateSmsModuleDAO implements SmsModuleDAO {
	
	protected Log log = LogFactory.getLog(getClass());
	protected SessionFactory session;

	/**
	 * @param session the session to set
	 */
	public void setSessionFactory(SessionFactory session) {
		this.session = session;
	}

	/**
	 * @see org.openmrs.module.sms.db.SmsDAO#createProvider(org.openmrs.module.sms.model.SmsProvider)
	 */
	public SmsProvider createProvider(SmsProvider provider) {
		SmsProvider newProvider = provider;
		session.getCurrentSession().saveOrUpdate(newProvider);
		return newProvider; 
	}

	
	/**
	 * @see org.openmrs.module.sms.db.SmsDAO#deleteProvider(org.openmrs.module.sms.model.SmsProvider)
	 */
	public void deleteProvider(SmsProvider provider) {
		session.getCurrentSession().delete(provider);
	}

	/**
	 * @see org.openmrs.module.sms.db.SmsDAO#getAllProviders()
	 */
	public List<SmsProvider> getAllProviders() {
		return session.getCurrentSession().createCriteria(SmsProvider.class).list();
	}
	
	/**
	 * @see org.openmrs.module.sms.db.SmsDAO#getActiveProviders()
	 */
	public List<SmsProvider> getActiveProviders(){
		return session.getCurrentSession()
		.createQuery("FROM SmsProvider AS b where b.voided <> 0").list();
	}

	/**
	 * @see org.openmrs.module.sms.db.SmsDAO#getPreferredProvider()
	 */
	public List<SmsProvider> getPreferredProvider() {
		return session.getCurrentSession()
		.createQuery("FROM SmsProvider AS b where b.preferred = 1 AND  b.voided <> 1").list();
	}

	/**
	 * @see org.openmrs.module.sms.db.SmsDAO#getProviderById(java.lang.String)
	 */
	public SmsProvider getProviderById(Integer id) {
		return (SmsProvider)session.getCurrentSession()
		.createQuery("FROM SmsProvider AS b WHERE  b.id = '"+id+"'").uniqueResult();
	}

	/**
	 * @see org.openmrs.module.sms.db.SmsDAO#createMessage(org.openmrs.module.sms.model.SmsProgramming)
	 */
	public SmsProgramming createMessage(SmsProgramming programming) {
		SmsProgramming prg = programming;
		session.getCurrentSession().saveOrUpdate(prg);
		return prg;
	}

	/**
	 * @see org.openmrs.module.sms.db.SmsDAO#deteleMessage(org.openmrs.module.sms.model.SmsProgramming)
	 */
	public void deteleMessage(SmsProgramming programming) {
		session.getCurrentSession().delete(programming);
	}

	/**
	 * @see org.openmrs.module.sms.db.SmsDAO#getAllMessages()
	 */
	public List<SmsProgramming> getAllMessages() {
		return session.getCurrentSession().createCriteria(SmsProgramming.class).list();
	}

	/**
	 * @see org.openmrs.module.sms.db.SmsDAO#getMessagesByKeyWord(java.lang.String)
	 */
	public List<SmsProgramming> getMessagesByKeyWord(String key) {
		return session.getCurrentSession()
		.createQuery("FROM SmsProgramming AS b WHERE b.name LIKE '%"+key+"%'").list();
	}
	
	public SmsProgramming getMessageById(Integer id){
		return (SmsProgramming)session.getCurrentSession()
		.createQuery("FROM SmsProgramming AS b WHERE b.id='"+id+"'").uniqueResult();
	}
	
	public void createHistory(SmsHistory history){
		session.getCurrentSession().saveOrUpdate(history);
	}

	//return if this message has been sent to this patient
	public SmsHistory getHistory(Patient patient, SmsProgramming programming){
		SmsHistory result = null;
		List<SmsHistory> list = session.getCurrentSession()
				.createQuery("FROM SmsHistory AS b WHERE b.programmingId = '"+programming.getId()+"' and b.patientId = '" +patient.getId() +"' ORDER BY b.id DESC").list();
		
		if (list != null && list.size() > 0){
			result = list.get(0);
		}
		
		return result;

	}
	
	
	//return if the list message has been sent to this patient
	public List<SmsHistory> getListHistory(Patient patient, SmsProgramming programming){
		List<SmsHistory> list = session.getCurrentSession()
				.createQuery("FROM SmsHistory AS b WHERE b.programmingId = '"+programming.getId()+"' and b.patientId = '" +patient.getId() +"' ORDER BY b.id DESC").list();
		return list;
	}
        
        // return the list message archived for the program
        public List<SmsHistory> getListHistory(SmsProgramming programming){
		List<SmsHistory> list = session.getCurrentSession()
				.createQuery("FROM SmsHistory AS b WHERE b.programmingId = '"+programming.getId() + "' ORDER BY b.patientId ASC").list();
		return list;
	}
	
	//return the list observation of one patient and one concept_id
	public List<Integer> getListObservationFromConceptAndPatient(Patient patient, SmsProgramming programming){
		List<Integer> list = session.getCurrentSession()
				.createQuery("Select obsId FROM Obs AS o WHERE o.concept.conceptId = '"+programming.getConcept()+"' and o.person.personId = '" +patient.getId() +"'").list();
		return list;
	}
        
        // return the list observation of one concept_id
    public List getListObservationFromConcept(SmsProgramming programming){
    	String sql = "select obs_id, person_id from obs where (voided is null or voided = false) and concept_id=" + programming.getConcept() + " order by person_id ASC";
        SQLQuery query = session.getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List result = query.list();
        return result;
	}
    
    public List getListObservationFromProgramming(SmsProgramming programming, List<Integer> listPatient, boolean sendRepeat){
    	if(listPatient == null || listPatient.size() == 0) return null;
    	//log.error("in getlistobs a");
    	int concept_id = 0;
    	try{
    		concept_id = programming.getConcept();
        }catch (NullPointerException e) {
        	log.error("usando SQL para evento que no funciona");
        }

    	//log.error("in getlistobs b");
    	int programming_id = programming.getId();
    	StringBuilder builder = new StringBuilder("select obs_id,person_id from obs where (voided is null or voided = false)");
    	builder.append(" and concept_id=" + concept_id);
    	builder.append(" and obs_id not in (");
    	builder.append("select obs_id from sms_history where programming_id="+ programming_id);
    	if(sendRepeat){
    		builder.append(" and sent_to_gateway_result='success'");
    	}
    	builder.append(") and person_id in (");
    	for(int i = 0; i < listPatient.size(); i++){
    		builder.append(listPatient.get(i).toString());
    		if(i < listPatient.size() - 1){
    			builder.append(",");
    		}
    	}
    	builder.append(") order by person_id ASC");
    	
    	String sql = builder.toString();
    	//log.error("in getlistobs sql: " +sql);
  	
    	SQLQuery query = session.getCurrentSession().createSQLQuery(sql);
    	//log.error("in getlistobs d");

    	query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
    	//log.error("in getlistobs e");

    	List result = query.list();
        return result;
    }
    
    public void deleteAllSmsQueue(){
    	String sql = "delete from sms_queue";
    	SQLQuery query = session.getCurrentSession().createSQLQuery(sql);
    	query.executeUpdate();
    	
    }	
	
	public List<SmsQueue> getAllSmsQueue(){
		return session.getCurrentSession().createCriteria(SmsQueue.class).list();
	}
	
	public SmsQueue getSmsQueueById(Integer id){
		return (SmsQueue)session.getCurrentSession()
				.createQuery("FROM SmsQueue AS b WHERE b.id="+id).uniqueResult();
	}
	
	public void saveSmsQueue(SmsQueue smsQueue){
		session.getCurrentSession().saveOrUpdate(smsQueue);
	}
	
	public void deleteSmsQueue(SmsQueue smsQueue){
		session.getCurrentSession().delete(smsQueue);
	}

	@Override
	public Date getDateSQL(String sql, Integer patientId) {
//		sql = escapeComment(sql + " and patient_id=" + patientId + ";");
		// I'm not sure why you changed the code into this, because
		// it should have the word 'and' to connect the two conditions
		sql = escapeComment(sql +patientId + ";");
		List<SimpleDateFormat> dateFormats = new ArrayList();
		dateFormats.add(new SimpleDateFormat("yyyy-MM-dd"));
		dateFormats.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

		SQLQuery query = session.getCurrentSession().createSQLQuery(sql);
		try {
			Object result = query.uniqueResult();
			if (result instanceof Date){
				//log.error("sqlDate is instanceof Date, returning: " +result);
				return (Date)result;
			}else if(result == null){
				//log.error("sqlDate is null: " +result);
				return null;
			}else{
				Date endDate = null;
				for (int i = 0 ; i < dateFormats.size(); i++) {
					SimpleDateFormat format=dateFormats.get(i);
				    boolean finished = true;
					//logger.error("format chosen: " +format);
			    	try {
		                format.setLenient(false);
		                endDate = format.parse(result.toString());
		                finished=true;
			        } catch (ParseException err) {
			                //Shhh.. try other formats
							finished=false; //probably not used but never hurts
			        }
			        if (finished) {
		                break;
			        }
				}
				//log.error("parsing sqlDate:" +endDate);
				return endDate;
			}
		} catch (HibernateException e){
			log.error("HibernateException in getting sqlDate: " +e.getMessage());
		}
		return null;
	}

	private String escapeComment(String sql) {
		
		return sql;
	}
	
}
