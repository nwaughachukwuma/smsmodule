/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms.web.dwr;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.module.reporting.cohort.definition.CohortDefinition;
import org.openmrs.module.reporting.cohort.definition.service.CohortDefinitionService;

/**
 * A simple DWR service class to get CohorotDefinitions, to be showed in create message JSP page.
 * @author crecabarren
 * @since 0.1
 */
public class DWRCohortDefinitionService {
	
	private Log log = LogFactory.getLog(DWRCohortDefinitionService.class); 
	
	/**
	 * 
	 * @return
	 */
	public List<CohortDefinition> getCohorts(){
		List<CohortDefinition> cohorts;
		CohortDefinitionService cService = Context.getService(CohortDefinitionService.class);
		cohorts = cService.getAllDefinitions(false);
		for (CohortDefinition c : cohorts){
			log.info("I've gotten that cohort "+c.getName());
		}
		return cohorts;
	}
}
