/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms.web.dwr;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.transaction.ResinTransactionManagerLookup;
import org.openmrs.Concept;
import org.openmrs.Patient;
import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.module.reporting.cohort.definition.CohortDefinition;
import org.openmrs.module.reporting.cohort.definition.service.CohortDefinitionService;
import org.openmrs.module.sms.api.SmsService;
import org.openmrs.module.sms.model.PatientListSmsTestListItem;
import org.openmrs.module.sms.model.SmsProgramming;
import org.openmrs.module.sms.send.SendSms;

/**
 * A DWR Service, to provide a JavaScript class to be used in a JSP page.
 * @author crecabarren
 * @since 0.1
 */
public class DWRSmsProgrammingService {
	
	protected Log log = LogFactory.getLog(getClass());
	private SmsService service = Context.getService(SmsService.class);

	/**
	 * A method for get messages to be displayed in a JSP page.
	 * @return
	 */
	public List<SmsProgrammingListItem> getMessages(){
		List<SmsProgrammingListItem> messages = new ArrayList<SmsProgrammingListItem>();
		if(Context.isAuthenticated()){
			for (SmsProgramming item : service.getAllMessages()){
				messages.add(new SmsProgrammingListItem(item));
			}
		}
		return messages;
	}
	
	/**
	 * 
	 * @param cohortId
	 * @param conceptId
	 * @param messageList
	 * @return
	 */
	public List<SmsProgrammingListItem> saveOrUpdate(Integer cohortId, Integer conceptId,
			List<SmsProgrammingListItem> messageList){
		List<SmsProgrammingListItem> list = new ArrayList<SmsProgrammingListItem>();
		SmsProgramming programming;		
		
		if(Context.isAuthenticated()){
			for (SmsProgrammingListItem obj : messageList){
				if(obj.getId() != null){
					programming = service.getMessageById(obj.getId());
				}
				else {
					programming = new SmsProgramming();	
				}
				programming.setName(obj.getName());
				programming.setText(obj.getText());
				programming.setDelay(obj.getDelay());
				programming.setEvent(obj.getEvent());
				if (cohortId != null){
					CohortDefinition cohort = Context.getService(CohortDefinitionService.class)
											.getDefinition(CohortDefinition.class, cohortId);
					programming.setCohort(cohort.getId());
				}
				if (conceptId != null){
					Concept concept = Context.getConceptService().getConcept(conceptId);
					programming.setEvent(concept.getName().getName());
					programming.setConcept(concept.getConceptId());
				}
				programming.setEventSQL(obj.getSql());
				service.createMessage(programming);
				list.add(new SmsProgrammingListItem(programming));
			}
		}
		return list;
	}
	
	/**
	 * 
	 * @param item
	 */
	public void deleteMessage(List<SmsProgrammingListItem> messages){
		if(Context.isAuthenticated()){
			for (SmsProgrammingListItem message : messages){
				SmsProgramming toBeDelete = service.getMessageById(message.getId());
				service.deteleMessage(toBeDelete);
			}
		}
	}
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	public String getDateSQL(String sql, Integer patientId){
		String result = "";
		Date date = service.getDateSQL(sql, patientId);		
		if (date != null){
			result = sdf.format(date);
		}
		return result;
	}
	
	public void sendMessage(Integer smsMessageId, Integer patientId)
	{
		SendSms.sendMessage(smsMessageId, patientId);
	}
	
	@SuppressWarnings("deprecation")
	public List<PatientListSmsTestListItem> getPatients(String query)
	{
		PatientService patientService = Context.getService(PatientService.class);
		List<Patient> result;
		List<PatientListSmsTestListItem> list = new ArrayList<PatientListSmsTestListItem>();
		if (query.isEmpty())
		{
			result = patientService.getAllPatients();
		}else{
			result = patientService.findPatients(query, false);
		}
		
		for (Patient model : result)
		{
			PatientListSmsTestListItem curItem = new PatientListSmsTestListItem();
			curItem.setId(model.getId());
			curItem.setPatientId(model.getPatientId());
			curItem.setPersonName(model.getPersonName().toString());
			curItem.setDisplayName(model.getPatientIdentifier().getIdentifier() +
					" " + curItem.getPersonName() + " (" + model.getAge() + " yo)");
			list.add(curItem);
		}
		
		return list;
	}
	
	public boolean getViewSMSTestPermission(String permission)
	{
		return Context.hasPrivilege(permission);
	}
}
