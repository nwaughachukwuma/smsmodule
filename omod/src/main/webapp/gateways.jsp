<%--
Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.

Reproduction or distribution of this source code is prohibited.
--%>
<%@ include file="/WEB-INF/template/include.jsp" %>

<script>

function makeGatewaysManagementPanel(){
	return new Ext.Panel({
		frame : true,
		border : false,
		autoHeight : true,
		autoWidth : true,
		id : 'gateways',
		bodyStyle : "padding: 5px",
		layout : "column",
		items : [{
			columnWidth : 0.6,
			layout: 'fit',
			items: [{
				id : 'gateway-list',
				xtype : "grid",
				title : '<spring:message javaScriptEscape="true" code="sms.current.gateways" />',
				autoHeight: true,
				colModel : new Ext.grid.ColumnModel([
					{id : 'gateway-list-id', header : '<spring:message javaScriptEscape="true" code="sms.id" />', width : 30, sortable : false, dataIndex : 'id'},
					{id : 'gateway-list-name', header : '<spring:message javaScriptEscape="true" code="sms.name" />', width : 80, sortable : false, dataIndex : 'name'},
					{id : 'gateway-list-url', header : '<spring:message javaScriptEscape="true" code="sms.url" />', width : 200, sortable : false, dataIndex : 'address'},
					{id : 'gateway-list-preferred', header : '<spring:message javaScriptEscape="true" code="sms.preferred" />', width : 75, sortable : false, dataIndex : 'preferred'},
					{id : 'gateway-list-voided', header : '<spring:message javaScriptEscape="true" code="sms.voided" />', width : 50, sortable : false, dataIndex : 'voided'}]),
				store : Ext.StoreMgr.lookup('gateway-store'),
				selModel : new Ext.grid.RowSelectionModel({
					singleSelect: true,
					listeners: {
						rowselect: function(sm, row, rec) {
							Ext.getCmp("gateway-form-name").enable();
							Ext.getCmp("gateway-form-username").enable();
							Ext.getCmp("gateway-form-password").enable();
							Ext.getCmp("gateway-form-address").enable();
							Ext.getCmp("gateway-form").getForm().loadRecord(rec);
							Ext.getCmp("gateway-form-preferred").enable();
							Ext.getCmp("gateway-form-save").enable();
							Ext.getCmp("gateway-form-delete").enable();
						}
					}
				})
			}]
		},{
			columnWidth : 0.4,
			xtype : "form",
			id : "gateway-form",
			name : "gateway-form",
			layout : "form",
			items : [{
				xtype : "hidden",
				id : "gateway-form-id",
				name : "id"
			},{
				xtype : "fieldset",
				title : '<spring:message javaScriptEscape="true" code="sms.add.a.new.gateway" />',
				border : false,
				autoHeight : true,
				autoWidth : true,
				defaultType : "textfield",
				items : [{
					fieldLabel : '<spring:message javaScriptEscape="true" code="sms.name" />',
					id : "gateway-form-name",
					name : "name"
				},{
					fieldLabel : '<spring:message javaScriptEscape="true" code="sms.user" />',
					id : "gateway-form-username",
					name : "username"
				},{
					fieldLabel : '<spring:message javaScriptEscape="true" code="sms.password" />',
					id : "gateway-form-password",
					name : "password",
					inputType : "password"
				},{
					fieldLabel : '<spring:message javaScriptEscape="true" code="sms.url" />',
					id : "gateway-form-address",
					name : "address",
				},{
					xtype : "checkbox",
					fieldLabel : '<spring:message javaScriptEscape="true" code="sms.preferred" />',
					id : "gateway-form-preferred",
					name : "preferred"
				},{
					xtype : "checkbox",
					fieldLabel : '<spring:message javaScriptEscape="true" code="sms.void" />',
					id : "gateway-form-voided",
					name : "voided"
				}]
			}],
			buttons : [{
				id : "gateway-form-new",
				text : '<spring:message javaScriptEscape="true" code="sms.new" />',
				disabled : false,
				handler : function(){
					Ext.getCmp("gateway-list").getSelectionModel().clearSelections();
					Ext.getCmp("gateway-form").getForm().reset();
					Ext.getCmp("gateway-form-name").enable();
					Ext.getCmp("gateway-form-username").enable();
					Ext.getCmp("gateway-form-password").enable();
					Ext.getCmp("gateway-form-address").enable();
					Ext.getCmp("gateway-form-preferred").enable();
					Ext.getCmp("gateway-form-voided").enable();
					Ext.getCmp("gateway-form-save").enable();
					Ext.getCmp("gateway-form-delete").enable();
				}
			},{
				id : "gateway-form-save",
				text : '<spring:message javaScriptEscape="true" code="sms.save" />',
				disabled : true,
				handler : function(){
					if(Ext.getCmp("gateway-form").getForm().isValid()){
						var store = Ext.StoreMgr.lookup('gateway-store');

						if(Ext.getCmp("gateway-form-id").getValue() != ""){
							var record = store.getById(Ext.getCmp("gateway-form-id").getValue());
							record.beginEdit();
							record.set("name", Ext.getCmp("gateway-form-name").getValue());
							record.set("username", Ext.getCmp("gateway-form-username").getValue());
							record.set("password", Ext.getCmp("gateway-form-password").getValue());
							record.set("address", Ext.getCmp("gateway-form-address").getValue());
							record.set("preferred", Ext.getCmp("gateway-form-preferred").getValue());
							record.set("voided", Ext.getCmp("gateway-form-voided").getValue());
							record.endEdit();
							record.commit();
						}
						else{
							var arr = new store.recordType({
								name : Ext.getCmp("gateway-form-name").getValue(),
								username : Ext.getCmp("gateway-form-username").getValue(),
								password : Ext.getCmp("gateway-form-password").getValue(),
								address : Ext.getCmp("gateway-form-address").getValue(),
								preferred : Ext.getCmp("gateway-form-preferred").getValue(),
								voided : Ext.getCmp("gateway-form-voided").getValue()
							});
							try{
								store.add(arr);
							}catch(e){alert(e.toSource());}
						}
					}
				}
			},{
				id : "gateway-form-delete",
				text : '<spring:message javaScriptEscape="true" code="sms.reset" />',
				disabled : true,
				handler : function(){
					Ext.MessageBox.confirm('Confirm', '<spring:message javaScriptEscape="true" code="sms.confirm.delete.provider" />', function(btn) {
						if (btn == 'yes') {
							var store = Ext.StoreMgr.lookup('gateway-store');
							if(Ext.getCmp("gateway-form-id").getValue() != ""){
								var record = store.getById(Ext.getCmp('gateway-form-id').getValue());
								store.remove(record);
							}
							else{
								Ext.getCmp("gateway-form").getForm().reset();
							}
						}
					});
				}
			}]
		}]
	});
}
</script>