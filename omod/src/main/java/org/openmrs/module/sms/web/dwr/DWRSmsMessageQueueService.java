package org.openmrs.module.sms.web.dwr;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.module.sms.api.SmsService;
import org.openmrs.module.sms.model.SmsQueue;

public class DWRSmsMessageQueueService {
	protected Log log = LogFactory.getLog(getClass());
	private SmsService service = Context.getService(SmsService.class);
	
	/**
	 * Return a list of message in the sms queue
	 * @return
	 */
	public List<SmsQueueListItem> getSmsQueues(){
		List<SmsQueueListItem> list = new ArrayList<SmsQueueListItem>();
		
		if(Context.isAuthenticated()){
			for(SmsQueue queue : service.getAllSmsQueue()){
				list.add(new SmsQueueListItem(queue));
			}
		}
		return list;
	}
	
	public void deleteMessageFromQueue(List<SmsQueueListItem> delItems){
		if(Context.isAuthenticated()){
			for (SmsQueueListItem delItem : delItems){
				service.deleteSmsQueue(service.getSmsQueueById(delItem.getId()));
			}
		}
	}
	
	
}
