package org.openmrs.module.sms.util;

public class SMSConstants {

		//global properties
		public static final String GP_MAIL_USER = "mail.user";
		public static final String GP_MAIL_PASSWORD = "mail.password";
		public static final String GP_MAIL_PROTOCOL = "mail.transport.protocol";
		public static final String GP_MAIL_HOST = "mail.smtp.host";
		public static final String GP_MAIL_PORT = "mail.smtp.port";
		public static final String GP_MAIL_AUTH = "mail.smtp.auth";
		public static final String GP_MAIL_FROM = "mail.smtp.from";
		public static final String GP_MAIL_STARTTLS_ENABLE="mail.smtp.starttls.enable";
		
		public static final String GP_REPEAT_ALERTS = "programmablealerts.repeat_alerts";
		public static final String GP_SEND_ATTEMPTS = "programmablealerts.send_attempts";
		
		//global properties values
		public static final String GP_MAIL_PROTOCOL_VALUE = "mail.transport_protocol";
		public static final String GP_MAIL_HOST_VALUE = "mail.smtp_host";
		public static final String GP_MAIL_PORT_VALUE = "mail.smtp_port";
		public static final String GP_MAIL_AUTH_VALUE = "mail.smtp_auth";
		public static final String GP_MAIL_FROM_VALUE = "mail.from";
		public static final String GP_MAIL_STARTTLS_ENABLE_VALUE="mail.smtp_starttls_enable";
		
		public static String errorMessage;
		
}
