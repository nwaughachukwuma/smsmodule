
<%@ include file="/WEB-INF/template/include.jsp" %>

<script>
var storeLoaded = false;
function objectFound(patients){
	storeLoaded = true;
	
	var store = Ext.StoreMgr.lookup('patient-store');
	Ext.each(patients, function(patient, idx){
		var record = new store.recordType({patientId: patient.patientId, 
			personName: patient.personName,
			displayName: patient.displayName});
		store.add(record);
	});
	
	store.commitChanges();
	try{
		Ext.MessageBox.hide();
	}catch(err){}
}


function makeMessageTestPanel(){
	var patientQueryTask = new Ext.util.DelayedTask(function(){
		Ext.getCmp('patient-type').getStore().removeAll();
		storeLoaded = false;
		var query = Ext.getCmp('patient-type').getRawValue();
		if (query.length > 2){
			DWRSmsProgrammingService.getPatients(query, objectFound);
		}
	 });
	
	return new Ext.Panel({
		frame: true,
		border: false,
		bodyStyle: 'padding: 0px',
		autoHeight: true,
		autoWidth: true,
		items: [
	        {xtype : "form",
			id : "smsTest-form",
			name : "smsTest-form",
			layout : "form",
			items : [
			{
				id: 'patient-type',
				xtype: 'combo',
				fieldLabel: '<spring:message javaScriptEscape="true" code="sms.patient.name" />',
				typeAhead: true,
				enableKeyEvents: true,
				lazyRender:true,
				forceSelection: true,
			    triggerAction: 'all',
			    emptyText: '<spring:message javaScriptEscape="true" code="sms.enter.patient.name" />',
				mode: 'local',
				width: 430,
			    store: Ext.StoreMgr.lookup('patient-store'),
			    valueField: 'patientId',
			    displayField: 'displayName',
			    listeners:{
			    	select: function(){
			    		var store = this.getStore();
						var idx = store.find('patientId', this.getValue());
						var personName = store.getAt(idx).get('personName');
						this.setRawValue(personName);
						if (Ext.getCmp('message-type').getValue() != '')
			    			Ext.getCmp('smsTest-form-send').enable();
						else{
							Ext.getCmp('smsTest-form-send').disable();
						}
			    	},
			    	blur: function(){
			    			var store = this.getStore();
			    			var idx = store.find('patientId', this.getValue());
			    			if(idx != -1){
				    			var personName = store.getAt(idx).get('personName');
				    			this.setRawValue(personName);
			    			}
			    			if (Ext.getCmp('message-type').getValue() != '')
				    			Ext.getCmp('smsTest-form-send').enable();
							else{
								Ext.getCmp('smsTest-form-send').disable();
							}
			    	},
			    	keydown: function(){
			    		patientQueryTask.delay(200);
			    		if (Ext.getCmp('message-type').getValue() != '')
			    			Ext.getCmp('smsTest-form-send').enable();
						else{
							Ext.getCmp('smsTest-form-send').disable();
						}
			    	},
			    	expand: function(){
			    		
			    		if(!storeLoaded){
				    		Ext.MessageBox.show({
				                msg: 'the store is loading, please wait...',
				                progressText: 'Loading...',
				                width:300,
				                wait:true,
				                waitConfig: {interval:200},
				                animateTarget: 'waitButton'
				             });
			    		}
			    	},
			    	change: function(){
			    		if (Ext.getCmp('message-type').getValue() != '')
			    			Ext.getCmp('smsTest-form-send').enable();
						else{
							Ext.getCmp('smsTest-form-send').disable();
						}
			    	}
			    }
			},
	        {
				id: 'message-type',
				xtype: 'combo',
				fieldLabel: '<spring:message javaScriptEscape="true" code="sms.message.name" />',
				typeAhead: true,
			    triggerAction: 'all',
			    lazyRender:true,
				mode: 'local',
			    store: Ext.StoreMgr.lookup('message-store'),
			    valueField: 'id',
			    displayField: 'name',
			    enableKeyEvents: true,
			    listeners:{
			    	select: function(){
			    		if (Ext.getCmp('patient-type').getValue() != '')
			    			Ext.getCmp('smsTest-form-send').enable();
			    		else{
							Ext.getCmp('smsTest-form-send').disable();
						}
			    	},
			    	change: function(){
			    		if (Ext.getCmp('patient-type').getValue() != '')
			    			Ext.getCmp('smsTest-form-send').enable();
						else{
							Ext.getCmp('smsTest-form-send').disable();
						}
			    	}
			    }
			},
			{
				id : "smsTest-form-send",
				text : '<spring:message javaScriptEscape="true" code="sms.send" />',
				xtype: 'button',
				disabled: true,
				handler : function(){
					var patientId = Ext.getCmp('patient-type').getValue();
					var smsProgrammingId = Ext.getCmp('message-type').getValue();
					DWRSmsProgrammingService.sendMessage(parseInt(smsProgrammingId), parseInt(patientId));
					alert('<spring:message javaScriptEscape="true" code="sms.test.message.sent" />')
				}
			}
	        ]}
	]
});
}
</script>
		
		

