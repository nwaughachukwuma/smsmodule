/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms.send;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.Integer;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.security.*;
import java.sql.Timestamp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.stream.StreamSource;

import org.apache.axis2.AxisFault;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Cohort;
import org.openmrs.Concept;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.module.reporting.cohort.definition.CohortDefinition;
import org.openmrs.module.reporting.cohort.definition.service.CohortDefinitionService;
import org.openmrs.module.reporting.evaluation.EvaluationContext;
import org.openmrs.module.reporting.evaluation.EvaluationException;
import org.openmrs.module.sms.api.SmsService;
import org.openmrs.module.sms.model.SmsHistory;
import org.openmrs.module.sms.model.SmsProgramming;
import org.openmrs.module.sms.model.SmsProvider;
import org.openmrs.module.sms.model.SmsQueue;
import org.openmrs.module.sms.provider.celmediainfo.WSTelefonicaPushMTStub;
import org.openmrs.module.sms.util.EmailGateway;
import org.openmrs.notification.Message;
import org.springframework.stereotype.Component;


/**
 * A Class to perform the sms send, using the information of SmsProvider and SmsProgramming.
 * @author crecabarren
 * @author Samuel Mbugua
 * @since 0.1
 */
@Component
public class SendSms {
	
	protected static Log log;
	
	private static CohortDefinitionService cohortSrv;
	private static SmsService service;
	private static boolean sendRepeat = false;
	
	private static final String SEND_ERROR_SMS = "sms.send_error_sms";
	private static final String SEND_ERROR_SUBJECT = "sms.error_email_subject";
	private static final String SEND_ERROR_BODY = "sms.error_email_body";
	private static final String SEND_ERROR_RECIPIENT = "sms.error_recipient";
	private static final String SEND_ERROR_SENDER = "sms.error_sender";

	static private void init(){
		log = LogFactory.getLog(SendSms.class);
		cohortSrv = Context.getService(CohortDefinitionService.class);
		service = Context.getService(SmsService.class);
		sendRepeat = Boolean.valueOf(Context.getAdministrationService().getGlobalProperty("sms.repeat"));
	}
	
	static private void send(Patient patient, SmsProgramming message){
		SmsProvider provider;
		if ((provider = (SmsProvider) service.getPreferredProvider().get((int)Math.random()*service.getPreferredProvider().size())) != null){
			String host = provider.getAddress();
			//TextMagic
			//String query = "username="+provider.getUsername()+"&password="+provider.getPassword()+"&cmd=send&text="
			//+message.replace(' ', '+')+"&phone=569"+patient.getAttribute("Celular")+"&unicode=1";
			//Smartel
			//String query = "=ehschile&text="+message.replace(' ', '+')+"&to_number="+patient.getAttribute("Celular")
			//+"&username="+provider.getUsername()+"&password="+provider.getPassword();
			//TM4B
			//String query = "username="+provider.getUsername() +"&password="+provider.getPassword() +"&type=broadcast&version=2.1&msg=" +message.getText().replace(' ', '+')
			//+"&to=569"+patient.getAttribute("Celular") +"&from=12345678&data_type=plain";

			//Marketext
			String query = "api_id=lebox&user="+provider.getUsername() +"&password="+provider.getPassword() +"&text=" +message.getText().replace(' ', '+')
			+"&to=569"+patient.getAttribute("Celular"); // +"&from=12345678&data_type=plain";
			
			if (log != null)
				log.info("url = " + host + "?" + query);
			
			
			HttpURLConnection http = null;
		    OutputStreamWriter wr = null;
		    BufferedReader rd  = null;
		    StringBuilder sb = null;
		    String line = null;
		    URL url = null;

		      try {
				url = new URL(host);

				http = null;
				http = (HttpURLConnection) url.openConnection();
				http.setRequestMethod("POST");
				http.setDoOutput(true);
				http.setReadTimeout(10000);

				http.connect();
				
				//Not needed for GET, put needed for POST
				wr = new OutputStreamWriter(http.getOutputStream());
				wr.write(query);
				wr.flush();
				
				//and now I have to read the server response
				rd = new BufferedReader(new InputStreamReader(http.getInputStream()));
				sb = new StringBuilder();

				while ((line = rd.readLine()) != null)
		          {
		              sb.append(line + '\n');
		          }
				
				// Added by C. Recabarren. 2011, May 17th
				
				String broadcastID = "";
				String tmp = sb.toString();
				//log.error("SMS result is: " +tmp);
				String sentToGatewayResult = "";
				if(tmp.startsWith("ERR")){
					broadcastID = tmp;
					sentToGatewayResult = "failed";
				} else {
					String[] tmp1 = tmp.split(":");				
					if(tmp1 != null && tmp1.length > 1){
						//String[] tmp2 = tmp1[1].split(":");
						broadcastID = tmp1[1]; // it has to be added to history
						sentToGatewayResult = "success";
 					}
				}
				//				String[] tmp1 = tmp.split("<broadcastID>"); //TM4B
/*				if(tmp1 != null){
					//String[] tmp2 = tmp1[1].split(":");
					broadcastID = tmp1[1]; // it has to be added to history
				}
				else if (tmp != null){
					broadcastID = "error" +tmp;
					sentToGatewayResult = "failed";
				}
				else{
					broadcastID = "error";
					sentToGatewayResult = "failed";
				}
*/				
				Date date = new Date();
				java.sql.Timestamp date2 = new java.sql.Timestamp(date.getTime());
				
				SmsHistory history = new SmsHistory();
				history.setBroadcastId(broadcastID);
				history.setPatientId(patient.getId());
				history.setProgrammingId(message.getId());
				history.setProviderId(provider.getId());
				history.setSentToGatewayDate(date2);
				history.setSentToGatewayResult(sentToGatewayResult);
				
				service.createHistory(history);
				
				// End of modifications
				
		          //log.info("response is: " +sb.toString());
		      } catch (MalformedURLException e) {
		          e.printStackTrace();
		      } catch (ProtocolException e) {
		          e.printStackTrace();
		      } catch (IOException e) {
		          e.printStackTrace();
		      }
		      finally
		      {
		          //close the connection, set all objects to null
		          http.disconnect();
		          rd = null;
		          sb = null;
		          wr = null;
		          http = null;
		      }
		}
	}
	
	/*************************************************
	 * Added by Sam Mbugua for delay handling 
	 * @throws EvaluationException 
	 *************************************************/
	
	static public boolean sendMessage(SmsProgramming programming) throws EvaluationException{
//		init();
//		CohortDefinition cohort = cohortSrv.getDefinition(CohortDefinition.class, programming.getCohort());
//		Cohort c = cohortSrv.evaluate(cohort, new EvaluationContext());
//		List<Patient> patients = Context.getPatientSetService().getPatients(c.getMemberIds());		
//		for(Patient patient : patients){
//			//make sure it hasn't been sent before
//			List<SmsHistory> result = new ArrayList<SmsHistory>();			
//			result = service.getListHistory(patient, programming);
//			List<Integer> obsList = new ArrayList<Integer>();
//			obsList = service.}catch (AxisFault e) {getListObservationFromConceptAndPatient(patient, programming);
//			for (SmsHistory smsHistory: result){
//				int temp = -1;
//				temp = obsList.indexOf(smsHistory.getObsId());
//				if (temp != -1){
//					obsList.remove(temp);
//				}
//			}
//
//			//log.info("result is: " +result);
//			if(!obsList.isEmpty() || sendRepeat){
//				if (patient.getAttribute("Celular") != null && !"".equals(patient.getAttribute("Celular").getValue().trim())){
///*					if (patient.getId() == 98){
//						log.error("sending soap");
//						sendSOAP(patient, programming);
//					}
//*/					
//					log.debug("before " +patient.getFamilyName() +"send "+programming.getDelay() +" soap " + programming.getConcept());
//					
//					for (Integer obs_id : obsList){
//						if(sendToday(patient, programming, obs_id)){
//							SmsQueue smsQueue = new SmsQueue();
//							smsQueue.setPatientId(patient.getPatientId());
//							smsQueue.setProgrammingId(programming.getId());
//							smsQueue.setObsId(obs_id);
//							service.saveSmsQueue(smsQueue);                                                        
//						}
//						else
//							log.info(patient.getNames() + " has a message for another day");
//					}
//
//				} else {
//					log.info(patient.getNames() + " doesn't have a phone number");
//				}
//			}
//			
//		}
            
		init();
		CohortDefinition cohort = cohortSrv.getDefinition(CohortDefinition.class, programming.getCohort());
		Cohort c = cohortSrv.evaluate(cohort, new EvaluationContext());
		List<Patient> patients = Context.getPatientSetService().getPatients(c.getMemberIds());		

		List<Integer> patientIds = new ArrayList<Integer>();
        for(Patient patient : patients) patientIds.add(patient.getPatientId());
        
        /*
        List<SmsHistory> listSmsHistory = service.getListHistory(programming);
        List<Integer> listSmsHistory_obsId = new ArrayList<Integer>();
        for(SmsHistory smsHistory : listSmsHistory){
            listSmsHistory_obsId.add(smsHistory.getObsId());
        }
        */
        
        //List listObs = service.getListObservationFromConcept(programming);
/*        log.error("before get listobs");
        log.error("programming: " +programming);
        log.error("patientIds: " +patientIds);
        log.error("sendRepeat: " +sendRepeat);
*/        
        List listObs = service.getListObservationFromProgramming(programming, patientIds, sendRepeat);
//	        log.error("at listobs");
        
        if(listObs != null && !listObs.isEmpty()){
//        	log.error("listobs: " +listObs.toString());
        	
        	for(Object obj : listObs){
	            Map row = (Map) obj;
	            Integer obs_id = (Integer) row.get("obs_id");
	            Integer person_id = (Integer) row.get("person_id");
	            
	            int index = patientIds.indexOf(person_id);
	            if(index < 0) continue;
	            Patient patient = patients.get(index);
	            
	            /*
	            boolean sent = false;
	            index = listSmsHistory_obsId.indexOf(obs_id);
	            if(index >= 0){
	                if(sendRepeat){
	                    for(int i = 0; i < listSmsHistory_obsId.size(); i++){
	                        if(listSmsHistory_obsId.get(i).equals(obs_id) && listSmsHistory.get(i).getSentToGatewayResult().equalsIgnoreCase("success")){
	                            sent = true;
	                            break;
	                        }
	                    }
	                }else{
	                    sent = true;
	                }
	            }
	            if(sent) continue;
	            */
	            
	            if (patient.getAttribute("Celular") != null && !"".equals(patient.getAttribute("Celular").getValue().trim())){
	                    log.debug("before " +patient.getFamilyName() +"send "+programming.getDelay() +" soap " + programming.getConcept());
	
	                    if(sendToday(patient, programming, obs_id)){
	                            SmsQueue smsQueue = new SmsQueue();
	                            smsQueue.setPatientId(patient.getPatientId());
	                            smsQueue.setProgrammingId(programming.getId());
	                            smsQueue.setObsId(obs_id);
	                            service.saveSmsQueue(smsQueue);                                                        
	                    }
	                    else{
	                            log.error(patient.getNames() + " has a message for another day 2");
	                    }
	
	            } else {
	                    log.error(patient.getNames() + " doesn't have a phone number");
	            }                        
	        }
        } else{
        	log.error("listobs was null or empty");
        }
                
		List<SmsQueue> messages = service.getAllSmsQueue();
		for(SmsQueue smsQueue : messages){
			if(sendNow(smsQueue)){
				sendSOAP(smsQueue);
			}
		}
		
		return false;
	}
	
	private static boolean sendToday(Patient patient, SmsProgramming program, Integer obsId) {
		ConceptService cs = Context.getConceptService();
		Integer delay = program.getDelay(); 
		//log.error("delay: " +delay);
		Integer conceptId = program.getConcept();
		//log.error("in sendtoday");
		Date sqlDate = service.getDateSQL(program.getEventSQL(), patient.getId());
		
		if (delay == null && (conceptId == null || conceptId == 0) && sqlDate == null){
			//log.info("Either delay: " + delay + " is null or conceptId: " + conceptId + " is null or 0, so not sending messages");
			log.error("null values in sendToday");
			return false;
		}

		if (conceptId != null && conceptId == 6301){
			//log.info("conceptId: " + conceptId + " is 6301, so we're sending the message");
			log.error("should delete this");
			return true;
		}

		
		//check if concept is of date data type
		Concept concept = null;
		if (conceptId != null) {
			concept = cs.getConcept(conceptId);
			if ((concept == null || !concept.getDatatype().isDate()) && sqlDate == null){
				log.error("Not a date concept or a null concept so not sending");
				return false;
			}			
		}
		//now perform the actual delaying
		Date conceptDate = null;
		//log.error("obsId: " +obsId);
		conceptDate = getConceptDate(obsId);

		//log.info("patient is: " +patient +" concept is: " +concept);
		
		if (conceptDate == null && sqlDate == null){
			//log.info(patient.getNames() +" has no scheduled date so we will not send");
			log.error("no conceptDate in sendToday");
			return false;
		} else {
			//log.error("sqlDate: " +sqlDate);
			Date actualDate = sqlDate != null ? sqlDate : conceptDate;
			//log.error("actualDate: " +actualDate);
			long today = getDateWithoutTime(new Date());
			long thenTime = getDateWithoutTime(actualDate);
			long longDelay = (long) delay * 24 * 60 * 60 * 1000;
			long test = today-thenTime;
			
				// updated by Citigo
			long diff = today - thenTime;
			//log.error("calculated: " +test +" longDelay: " +longDelay +" Delay: " +delay +" diff " +diff);

			if (longDelay < 0) { //SMS should be sent before the date
				if (diff >= 0){
					return false; // if the delay is negative, and the time it should have sent has passed, then don't send
				} else if (diff >= longDelay) {
					return true; // if the delay is negative, and the time it should have sent is still in the future, but the difference between now and that time is less than the absolute value of delay, then send
				} else {
					return false; // if the delay is negative, and the time it should have sent is still in the future, but the difference between now and that time is greater than the absolute value of delay, then don't send
				}
			} else if (diff >= longDelay) {
				return true; // if the delay is positive, and the difference between the time it should have sent is more than delay, then send
			} else {
				return false;// if the delay is positive, and the difference between the time it should have sent is less than delay, then don't send
			}
			
		}
	}
	
	private static long getDateWithoutTime(Date date) {
		Calendar cal = Calendar.getInstance(Context.getLocale());
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}
	
	private static Date getConceptDate(Integer obsId) {
		//if (concept == null) return null;
		ObsService obs = Context.getObsService();
		//List<Obs> observations = obs.getObservationsByPersonAndConcept(patient, concept);
		Obs observation = obs.getObs(obsId);
		Date conceptDate = null;
		try {
			conceptDate = observation.getValueDatetime();
		}catch (Exception e) {
		}
		
		return conceptDate;
		
//		for (Obs observation : observations) {
//			if(observation.getId() == obsId)
//			Date obsDate = null;
//			try {
//				obsDate = observation.getValueDatetime();
//			}catch (Exception e) {
//				continue;
//			}
//
//			if(obsList.contains(observation.getId())){
//				conceptDates.add(obsDate);
//			}
//		}
//	
//		return conceptDates;
	}
	
	/*************************************************
	 * End of Sam's additions for delay handling 
	 *************************************************/
	
//	static public boolean sendMessage(SmsMessage message) throws Exception {
//		SmsProvider provider = message.getProvider();
//		String host = provider.getAddress();
//		String query = "username="+provider.getUsername()+"&password="+provider.getPassword()
//		+"&cmd=send&text="+message.getContent().replace(' ', '+')+"&phone="+message.getRecipient().getPhone()+"&unicode=1";
//		log.info("url = "+host+"?"+query);
//		
//		//here I make a http post request to the server.
//		URL url = new URL(host);
//		URLConnection http = url.openConnection();
//		http.setDoOutput(true);
//		OutputStreamWriter wr = new OutputStreamWriter(http.getOutputStream());
//		wr.write(query);
//		wr.flush();
//		
//		//and now I have to read the server response
//		BufferedReader rd = new BufferedReader(new InputStreamReader(http.getInputStream()));
//		String response = new String();
//		String line;
//		while((line = rd.readLine()) != null)
//			response += line;
//		rd.close();
//		wr.close();
//		log.info("response = "+response);
//		
//		
//		//and now I have to know if was a success or a fail.
//		JSONObject json = new JSONObject(response);
//		message.setSendAttempts(message.getSendAttempts()+1);
//		if(json.has("message_id")){
//			Date fecha = new Date();
//			Timestamp tm = new Timestamp(fecha.getTime());
//			message.setSentAt((Date)tm);
//			message.setStatus("sent");
//			return true;
//		}
//		else if(json.has("error_code")){
//			message.setStatus("fail");
//			log.equals("error_code = "+json.get("error_code"));
//			return false;
//		}
//		return false;
//	}
	/*public static void main(String[] args) {
		sendSOAP(null, null);
	}*/

	/**
	 * Function to send a sms message stored in the queue
	 * @param smsQueue
	 */
	static private void sendSOAP(SmsQueue smsQueue){
		// Update the number of attempt and the timestamp of the last attempt
		log.info("in sendSOAP");
		smsQueue.setNumberAttempt(smsQueue.getNumberAttempt() + 1);
		smsQueue.setLastAttemptDateTime(new Timestamp(new Date().getTime()));
		service.saveSmsQueue(smsQueue);
		
		init();
		SmsProgramming sms = service.getMessageById(smsQueue.getProgrammingId());
		Patient patient = Context.getPatientService().getPatient(smsQueue.getPatientId());
		Integer obs_id = smsQueue.getObsId();
		String msg = sms.getText();
		String number = "569" + patient.getAttribute("Celular");
            
        SmsHistory smsHistory = service.createSmsHistory(patient, sms, "", "", obs_id);

        //log.error("before attempting push");
        try {
        	WSTelefonicaPushMTStub stub = new WSTelefonicaPushMTStub();
        	//log.error("stub: " +stub.toString());
        	WSTelefonicaPushMTStub.EnvioSMS request = createRequest(number, msg);
        	//log.error("request: " +request.toString());
        	WSTelefonicaPushMTStub.EnvioSMSResponse response = stub.envioSMS(request);
        	//log.error("response: " +response.toString());
        	WSTelefonicaPushMTStub.WSData result = response.get_return();
        	//log.error("result:" +result.toString());
        	
        	smsHistory.setSentToGatewayResult("success");
        	smsHistory.setSmsSentResult(result.getDeserr());
        	if(smsQueue.getObsId().intValue()==0){
        		smsHistory.setSmsSentResultCode("Test Message");
        	}
        	log.info("envio de SMS exitoso: " +smsHistory.toString());
            service.createHistory(smsHistory);
            service.deleteSmsQueue(smsQueue);

		}catch (AxisFault e) {
			log.error("error in sending soap", e);
        	smsHistory.setSentToGatewayResult("failure");
            service.createHistory(smsHistory);
		} catch (RemoteException e) {
			log.error("error in sending soap", e);
        	smsHistory.setSentToGatewayResult("failure");
            service.createHistory(smsHistory);
		}finally{
			if(!smsHistory.getSentToGatewayResult().equalsIgnoreCase("success")){
				sendErrorEmail();
			}
				
		}
	}
	
	/**
	 * Check to see whether it has been 1 hour from the last time
	 * the system attempt to send this message or not
	 * if it is the case, return true, otherwise, return false
	 * @param smsQueue
	 * @return
	 */
	private static boolean sendNow(SmsQueue smsQueue){
		// if this is the first time the message was send
		// then try to send it anyway
		if(smsQueue.getLastAttemptDateTime() == null){
			return true;
		}
		
		// otherwise, check to see if it has been 1 hour or not from
		// the last time the system attempt to send that message 
		// to the current time.
		long lastAttemptTime = smsQueue.getLastAttemptDateTime().getTime();
		
		Date currentDate = new Date();
		long currentTime = currentDate.getTime();
		long threshold = Integer.parseInt(Context.getAdministrationService().getGlobalProperty("sms.timeToResend")) * 60 * 1000;
		if(currentTime - lastAttemptTime >= threshold)
			return true;
		
		return false;		
	}
	
	/*static public void test(){
		init();
		Set<Integer> ids = new HashSet<Integer>();
		ids.add(new Integer(2));
		List<Patient> list = Context.getPatientSetService().getPatients(ids);
		SmsProgramming sms = new SmsProgramming();
		sms.setText("test sms");
		sms.setId(1);
		service.createSmsHistory(list.get(0), sms, "coderr", "deserr");
		service.getHistory(list.get(0), sms);
	}*/

	
	static private void sendErrorEmail(){
		AdministrationService as = Context.getAdministrationService();
		String sendErrorSMS = as.getGlobalProperty(SEND_ERROR_SMS);
		if ("true".equals(sendErrorSMS)){								
			Message error = new Message();
			String content = as.getGlobalProperty(SEND_ERROR_BODY);
			error.setContent(content);
			error.setSender(as.getGlobalProperty(SEND_ERROR_SENDER));
			error.setSubject(as.getGlobalProperty(SEND_ERROR_SUBJECT));
			String recipient = as.getGlobalProperty(SEND_ERROR_RECIPIENT);				
			error.addRecipient(recipient);
			
			//------------------ 
			// June 3rd 
			// Try to replace this code with the send mail message stuff in programmablealerts module
			
//			try {
//				Context.getService(MessageService.class).setMessageSender(new MailMessageSender());
//				Context.getService(MessageService.class).sendMessage(error);
//			} catch (MessageException e1) {
//				log.error("Error send SMS email alert: " + e1.getMessage());
//				//e1.printStackTrace();
//			}
			
			EmailGateway gateway = new EmailGateway();
			gateway.startup();
			if(!gateway.sendMessage(as.getGlobalProperty(SEND_ERROR_SUBJECT), content, recipient)){
				log.error("Error send SMS email alert!");
			}
			gateway.shutdown();
			// ------------------------------------
			// end June 3rd modification
		}
	}
	static private WSTelefonicaPushMTStub.EnvioSMS createRequest(String number, String msg){
		WSTelefonicaPushMTStub.EnvioSMS sms = new WSTelefonicaPushMTStub.EnvioSMS();
		WSTelefonicaPushMTStub.WSDataIN data = new WSTelefonicaPushMTStub.WSDataIN();
		
		data.setNmovil(number);
		data.setMsgsms(msg);
		sms.setDatos(data);
		return sms;
	}
	
	static public void sendMessage(Integer smsMessageId, Integer patientId)
	{
		init();
		log.info("Sending test message to patient whose id is " + patientId + " and sms message id is: " + smsMessageId + " ...");
		/*if(service == null){
			log.error("in service is null");
			service = Context.getService(SmsService.class);
		}
*///		log.error("before queue");
		SmsQueue smsQueue = new SmsQueue();
	//	log.error("putting patinet ID " +patientId);
		smsQueue.setPatientId(patientId);
		//log.error("puttingsmsID " +smsMessageId);
		smsQueue.setProgrammingId(smsMessageId);
		smsQueue.setObsId(new Integer(0));
		//service.saveSmsQueue(smsQueue);
		//log.error("queue: " +smsQueue.toString());
		//log.error("programmingId: " +smsQueue.getProgrammingId());
		//log.error("patientId " +smsQueue.getPatientId());
		sendSOAP(smsQueue);
		
/*		SmsProgramming smsMessage = service.getMessageById(smsMessageId);
		PatientService patientService = Context.getService(PatientService.class);
		Patient patient = patientService.getPatient(patientId);
		send(patient, smsMessage);
*/
	}

}
